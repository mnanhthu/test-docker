const Note = require('../model/noteModel')
const noteCtrl ={
  createNote : async (req,res)=>{
    if(!req.body.title || !req.body.content)
    {
      res.status(400).json({
        msg:'Note cannot be empty',
        success: false
      })
    }
  
    const noteNew = new Note({
      title: req.body.title,
      content: req.body.content
    })
  
    try {
      const note = await noteNew.save()
      res.status(200).json({
        msg: "Created a note",
        sucess:true,
        note
      })
    } catch (err) {
      res.status(500).json({msg: err.message})
    }
  },

  getAllNote : async (req,res)=>{
    try {
      const notes = await Note.find()
      console.log(notes)
      res.status(200).json({
        sucess:true,
        notes:notes
      })
    } catch (err) {
      res.status(500).json({msg: err.message})
    }
  }
}


module.exports = noteCtrl