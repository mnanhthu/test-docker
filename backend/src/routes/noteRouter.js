const router = require('express').Router()
const noteCtrl = require('../controllers/noteCtrl')

router.post('/create',noteCtrl.createNote)
router.get('/get',noteCtrl.getAllNote)

module.exports = router