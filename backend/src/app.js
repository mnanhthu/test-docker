require('dotenv').config();
const cors = require('cors')
const mongoose = require('mongoose')
const express = require('express')
const noteRouter = require('./routes/noteRouter')

const app = express()


app.use(express.json())
app.use(cors())

const MONGO_HOST1= process.env.MONGO_HOST1
const MONGODB_PORT1 = process.env.MONGODB_PORT1

const MONGO_HOST2= process.env.MONGO_HOST2
const MONGODB_PORT2 = process.env.MONGODB_PORT2

const MONGO_HOST3= process.env.MONGO_HOST3
const MONGODB_PORT3 = process.env.MONGODB_PORT3

const MONGODB = `mongodb://${MONGO_HOST1}:${MONGODB_PORT1},${MONGO_HOST2}:${MONGODB_PORT2},${MONGO_HOST3}:${MONGODB_PORT3}/test`
mongoose.connect(MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, err => {
    if(err) throw err;
    console.log("Connected to mongodb")
})

/* const name = process.env.name || "World"

app.get('/name', (req, res) => {
    res.send(`Hello ${name} !`)
}) */
app.use('/note', noteRouter)


const PORT = process.env.PORT || 5000
app.listen(PORT, () => {
    console.log('Server is running on port', PORT)
})