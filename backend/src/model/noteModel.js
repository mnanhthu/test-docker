const mongoose = require('mongoose')


const noteSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true,'Title is require'],
        trim: true,
        unique: true
    },
    content:{
        type: String,
        required: [true,'Content is require'],
    }

}, {
    timestamps: true
})

module.exports = mongoose.model("Note", noteSchema)